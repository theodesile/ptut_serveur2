#include "Interface.h"
#include "Graph.h"
#include <vector>
#include <iostream>
#include <cmath>
#include "Car.h"
#include "Trigo.h"
#include "Point.h"
#include "Rotation.h"
#include <stdlib.h>

#ifdef WIN32
    #include <Windows.h>
    #define SLEEP Sleep
#else
    #include <unistd.h>
    #define SLEEP sleep
#endif

Interface::Interface(Application *application) : sf::RenderWindow(sf::VideoMode(1000, 1000), "Interface de test"){
	this->setFramerateLimit(60);
	m_application = application;
	m_lCar = application->getCars();
	start();

	
}

sf::Sprite loadSprite(sf::Texture* texture) {

	sf::Sprite sprite;
	sprite.setTexture(*texture);
	sprite.setScale(sprite.getScale().x / 36, sprite.getScale().y / 43);
	sprite.setOrigin(sprite.getLocalBounds().width / 2, sprite.getLocalBounds().height / 2);
	return sprite;

	
}
void Interface::start() {
	drawAllRoad();
	display();
	const float PI = 3.14159265359;
	
	bool isStop = true;
	texture = new sf::Texture();
	if (!texture->loadFromFile("sprites/Voiture1.png") 
		|| !m_backgroundTexture.loadFromFile("sprites/carteV1_mesures.png"))
	{
		std::cout << "merde les textures ont pas charge ! \n";
	}
	m_background.setTexture(m_backgroundTexture);
	const double scaleForBackground = 48.0 / 200.0;
	m_background.setScale(scaleForBackground, scaleForBackground);
	Point p1, p2;
	sf::CircleShape shape1(5), shape2(5);
	while (isOpen())
	{
		//m_application->getGraph()->printFile();
		m_lCar = m_application->getCars();
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
			isStop = !isStop;
		}
		  if (!isStop) {
			sf::Event e;
			clear(sf::Color(255, 255, 255));
			this->draw(m_background);
			//drawAllRoad();
			m_application->moveForward(0.5);
			for (int i = 0; i < m_lCar->size(); i++) {
				drawCar(&(m_lCar->at(i)));
			}
			display();

			SLEEP(1);

			while (pollEvent(e))
			{
				if (e.type == sf::Event::Closed)
				{
					close();
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			{
				sf::View v = this->getView();
				v.move(sf::Vector2f(-15, 0));
				this->setView(v);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			{
				sf::View v = this->getView();
				v.move(sf::Vector2f(15, 0));
				this->setView(v);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			{
				sf::View v = this->getView();
				v.move(sf::Vector2f(0, 15));
				this->setView(v);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			{
				sf::View v = this->getView();
				v.move(sf::Vector2f(0, -15));
				this->setView(v);
			}
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				sf::Vector2i posFen = this->getPosition();
				sf::Vector2i pos = sf::Mouse::getPosition();
				pos.x -= posFen.x;
				pos.y -= posFen.y+50;
				for (int i = 0; i < m_lCar->size(); i++) {
					Car* c = &(m_lCar->at(i));
					if (c->getX() < pos.x + 30 && c->getX() + 30 > pos.x
						&& c->getY() < pos.y + 30 && c->getY() + 30 > pos.y) {
						c->b = true;
						cout << "voiture fixee" << endl;
						for (int j = 0; j < m_lCar->size(); j++) {
							if (i != j)
								m_lCar->at(i).b = false;
						}
						break;

					}
				}
			}
		}
	}
}

void Interface::drawAllRoad() {
	Graph* graph = m_application->getGraph();
	int size = graph->getNodes().size();
	std::vector<Edge*> voisins;
	for (int i = 0; i < size; i++) {
		voisins = *graph->getNeightboor(i);
		for (int j = 0; j < voisins.size(); j++) {			
			drawRoad(voisins[j]);
		}
	}
}
void Interface::drawRoad(Edge* edge) {
	const float PI = 3.14159265359;
	int distance = edge->getLength();
	float x = edge->getX();
	float y = edge->getY();
	float radius = edge->getRadius();
	float angle = edge->getAngle();
	float angleToDraw;

	float x1, y1;
	float dangle = 0;
	sf::CircleShape shape(12);
	shape.setFillColor(sf::Color(23, 1627, 26));
	if (radius != 0) {
		dangle = 1 / radius;
		x -= cos(angle + PI / 2) * radius;
		y -= sin(angle + PI / 2) * radius;
	}
	for (float loop = 0; loop < distance; loop += 1) {
		if (radius != 0) {
			if (radius > 0)
				angleToDraw = angle - dangle * loop + PI / 2;
			else
				angleToDraw = angle - dangle * loop - PI / 2;
			//cout << "angle to draw = " << angleToDraw << endl;
			x1 = x + cos(angleToDraw) * abs(radius) - 12;
			y1 = y + sin(angleToDraw) * abs(radius) - 12;
		}
		else {
			x1 = x + cos(angle) * loop - 12;
			y1 = y + sin(angle) * loop - 12;
		}
		shape.setPosition(x1, y1);
		draw(shape);
		//angle += dangle  ;
	}
}

void Interface::drawCar(Car* car)
{
	sf::CircleShape shape(5);
	shape.setFillColor(sf::Color(255, 0, 0));
	sf::Sprite sprite = loadSprite(texture);
	sprite.setPosition(car->getX(), car->getY());
	sprite.setRotation((car->getAngle() + PI / 2) / (PI / 180));
	Rotation* r;
	for (int i = 0; i < car->getRotations().size(); i++) {
		r = &car->getRotations()[i];
		shape.setPosition(r->p1.x, r->p1.y);
		draw(shape);
		shape.setPosition(r->p2.x, r->p2.y);
		draw(shape);
	}
	draw(sprite);
}
