#include "Encodeur.h"

using namespace std;

string Encodeur::Decode(string chunk) {

    string data = Decompress(chunk);
    string decoded;

    unsigned char action = stoi(data.substr(data.length()-2, 2), nullptr, 2);
    unsigned char cityObject = stoi(data.substr(data.length()-5, 3), nullptr, 2);

    switch (action) {

        case ADD:
        case UPDATE:

            if(action == ADD)
                decoded = "ADD_";
            else
                decoded = "UPD_";

            switch (cityObject){

                case STRAIGHT_ROAD:
                case CURVED_ROAD:
                    decoded.append(DecodeRoad(data));
                    break;

                case CAR:
                case PEDESTRIAN:
                    decoded.append(DecodeCarPedestrian(data));
                    break;


                case FURNITURE:
                    decoded.append(DecodeFurniture(data));
                    break;

                case CAMERA:
                    decoded.append(DecodeCamera(data));
                    break;

                case CAR_WITH_PATH:
                    decoded.append(DecodeCarWithPath(data));
                    break;
            }

            break;

        case DELETE:
            decoded = "DEL_{";
            decoded += to_string(stoi(data.substr(0, data.length()-2), nullptr, 2));
            decoded += "}";

    }

    return decoded;
}

string Encodeur::Delete(unsigned short id) {
    string str;

    str = bitset<sizeof(id)*8 + 2>(DELETE | (id << 2)).to_string();
    str = Compress(str);

    return str;
}


// = = = COMPRESSION = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

string Encodeur::Compress(string data) {

    for (char c : data) if(c != '1' && c != '0') return nullptr;

    string compressed;
    for (int j = data.length()-6; j > -6; j-=6) {
        if(j >= 0)
            compressed += toascii( stoi(data.substr(j, sizeof(char)*8 - 2), nullptr, 2) + 63 );
        else
            compressed += toascii( stoi(data.substr(0, sizeof(char)*8 -2 +j), nullptr, 2) + 63 );
    }
    return compressed;
}

string Encodeur::Decompress(string data) {

    string decompressed;
    for(char c : data)
        decompressed = bitset<sizeof(c)*8 - 2>(c - 63).to_string() + decompressed;
    return  decompressed;

}


// = = = ROADS = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

string Encodeur::AddRoad(TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_LONGUEUR longueur, TYPE_SENS_UNIQUE sens_unique) {
    return EncodeRoad(ADD, NULL, STRAIGHT_ROAD, posx, posy, angle, longueur, NULL, sens_unique);
}

string Encodeur::AddRoad(TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_LONGUEUR longueur, TYPE_RAYON rayon, TYPE_SENS_UNIQUE sens_unique) {
    return EncodeRoad(ADD, NULL, CURVED_ROAD, posx, posy, angle, longueur, rayon, sens_unique);
}

string Encodeur::UpdateRoad(TYPE_ID id, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_LONGUEUR longueur, TYPE_SENS_UNIQUE sens_unique) {
    return EncodeRoad(UPDATE, id, STRAIGHT_ROAD, posx, posy, angle, longueur, NULL, sens_unique);
}

string Encodeur::UpdateRoad(TYPE_ID id, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_LONGUEUR longueur, TYPE_RAYON rayon, TYPE_SENS_UNIQUE sens_unique) {
    return EncodeRoad(UPDATE, id, CURVED_ROAD, posx, posy, angle, longueur, rayon, sens_unique);
}

string Encodeur::EncodeRoad(Action action, TYPE_ID id, CityObject type, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_LONGUEUR longueur, TYPE_RAYON rayon, TYPE_SENS_UNIQUE sens_unique){
    Flint fi;
    string str;

    if(action == UPDATE)
        str.append( bitset<sizeof(id)*8>(id).to_string() );

    fi.f = posx;
    str.append( bitset<sizeof(posx)*8>(fi.i).to_string() );
    fi.f = posy;
    str.append( bitset<sizeof(posy)*8>(fi.i).to_string() );
    str.append( bitset<sizeof(angle)*8>(angle).to_string() );
    str.append( bitset<sizeof(longueur)*8>(longueur).to_string() );

    if(type == CURVED_ROAD)
        str.append( bitset<sizeof(rayon)*8>(rayon).to_string() );

    if(sens_unique)
        str.append( "1");
    else
        str.append("0");

    str.append( bitset<3>(type).to_string() );
    str.append( bitset<2>(action).to_string() );

    return Compress(str);
}

string Encodeur::DecodeRoad(string data) {
    int action = stoi(data.substr(data.length() - 2, 2), nullptr, 2);
    int roadType = stoi(data.substr(data.length()-5, 3), nullptr, 2);

    Flint fi;
    char offset = data.length() - 5;

    offset -= 1;
    string sens_unique = to_string(stoi(data.substr(offset, 1), nullptr, 2));

    string rayon;
    if(roadType == CURVED_ROAD){
        offset -= sizeof(TYPE_RAYON)*8;
        rayon = to_string(stoi(data.substr(offset, sizeof(TYPE_LONGUEUR)*8), nullptr, 2));
    }

    offset -= sizeof(TYPE_LONGUEUR)*8;
    string longueur = to_string(stoi(data.substr(offset, sizeof(TYPE_LONGUEUR)*8), nullptr, 2));

    offset -= sizeof(TYPE_ANGLE)*8;
    string angle = to_string(stoi(data.substr(offset, sizeof(TYPE_ANGLE)*8), nullptr, 2));

    offset -= sizeof(TYPE_POS)*8;
    fi.l = stol(data.substr(offset, sizeof(TYPE_POS)*8), nullptr, 2);
    string posy = to_string(fi.f);

    offset -= sizeof(TYPE_POS)*8;
    fi.l = stol(data.substr(offset, sizeof(TYPE_POS)*8), nullptr, 2);
    string posx = to_string(fi.f);

    string id;
    if(action == UPDATE) {
        offset -= sizeof(TYPE_ID)*8;
        id = to_string(stoi(data.substr(offset, sizeof(TYPE_ID)*8), nullptr, 2));
    }

    string ret = to_string(roadType) + "{";
    if(action == UPDATE) ret.append(id + ",");
    ret.append(posx + "," + posy + "," + angle + "," + longueur);
    if(roadType == CURVED_ROAD) ret.append("," + rayon);
    return  ret + "," + sens_unique + "}";
}


// = = = CAR / PEDESTRIAN = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

string Encodeur::AddCar(TYPE_VARIANTE variante, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_VITESSE vitesse) {
    return EncodeCarPedestrian(ADD, NULL, CAR, variante, posx, posy, angle, vitesse);
}

string Encodeur::UpdateCar(TYPE_ID id, TYPE_VARIANTE variante, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_VITESSE vitesse) {
    return EncodeCarPedestrian(UPDATE, id, CAR, variante, posx, posy, angle, vitesse);
}

string Encodeur::AddPedestrian(TYPE_VARIANTE variante, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_VITESSE vitesse) {
    return EncodeCarPedestrian(ADD, NULL, PEDESTRIAN, variante, posx, posy, angle, vitesse);
}

string Encodeur::UpdatePedestrian(TYPE_ID id, TYPE_VARIANTE variante, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_VITESSE vitesse) {
    return EncodeCarPedestrian(UPDATE, id, PEDESTRIAN, variante, posx, posy, angle, vitesse);
}

string Encodeur::EncodeCarPedestrian(Action action, TYPE_ID id, CityObject type, TYPE_VARIANTE variante, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_VITESSE vitesse) {
    Flint fi;
    string str;

    if(action == UPDATE)
        str.append( bitset<sizeof(id)*8>(id).to_string() );

    str.append( bitset<sizeof(variante)*8>(variante).to_string() );
    fi.f = posx;
    str.append( bitset<sizeof(posx)*8>(fi.i).to_string() );
    fi.f = posy;
    str.append( bitset<sizeof(posy)*8>(fi.i).to_string() );
    str.append( bitset<sizeof(angle)*8>(angle).to_string() );
    str.append( bitset<sizeof(vitesse)*8>(vitesse).to_string() );
    str.append( bitset<3>(type).to_string() );
    str.append( bitset<2>(action).to_string() );

    return Compress(str);
}

string Encodeur::DecodeCarPedestrian(string data) {
    int action = stoi(data.substr(data.length() - 2, 2), nullptr, 2);
    int type = stoi(data.substr(data.length()-5, 3), nullptr, 2);

    Flint fi;
    int offset = data.length() - 5;

    offset -= sizeof(TYPE_VITESSE)*8;
    string vitesse = to_string(stoi(data.substr(offset, sizeof(TYPE_VITESSE)*8), nullptr, 2));

    offset -= sizeof(TYPE_ANGLE)*8;
    string angle = to_string(stoi(data.substr(offset, sizeof(TYPE_ANGLE)*8), nullptr, 2));

    offset -= sizeof(TYPE_POS)*8;
    fi.l = stol(data.substr(offset, sizeof(TYPE_POS)*8), nullptr, 2);
    string posy = to_string(fi.f);

    offset -= sizeof(TYPE_POS)*8;
    fi.l = stol(data.substr(offset, sizeof(TYPE_POS)*8), nullptr, 2);
    string posx = to_string(fi.f);

    offset -= sizeof(TYPE_VARIANTE)*8;
    string variante = to_string(stoi(data.substr(offset, sizeof(TYPE_VARIANTE)*8), nullptr, 2));

    string id;
    if(action == UPDATE) {
        offset -= sizeof(TYPE_ID)*8;
        id = to_string(stoi(data.substr(offset, sizeof(TYPE_ID)*8), nullptr, 2));
    }

    string ret = to_string(type) + "{";
    if(action == UPDATE) ret.append(id + ",");
    return ret.append(variante + "," + posx + "," + posy + "," + angle + "," + vitesse + "}");
}

// = = = FURNITURE = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

string Encodeur::AddFurniture(TYPE_VARIANTE variante, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle) {
    return EncodeFurniture(ADD, NULL, variante, posx, posy, angle);
}

string Encodeur::UpdateFurniture(TYPE_ID id, TYPE_VARIANTE variante, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle) {
    return EncodeFurniture(UPDATE, id, variante, posx, posy, angle);
}

string Encodeur::EncodeFurniture(Action action, TYPE_ID id, TYPE_VARIANTE variante, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle) {
    Flint fi;
    string str;

    if(action == UPDATE)
        str.append( bitset<sizeof(id)*8>(id).to_string() );

    str.append( bitset<sizeof(variante)*8>(variante).to_string() );
    fi.f = posx;
    str.append( bitset<sizeof(posx)*8>(fi.i).to_string() );
    fi.f = posy;
    str.append( bitset<sizeof(posy)*8>(fi.i).to_string() );
    str.append( bitset<sizeof(angle)*8>(angle).to_string() );
    str.append( bitset<3>(FURNITURE).to_string() );
    str.append( bitset<2>(action).to_string() );

    return Compress(str);
}

string Encodeur::DecodeFurniture(string data) {
    int action = stoi(data.substr(data.length() - 2, 2), nullptr, 2);

    Flint fi;
    char offset = data.length() - 5;

    offset -= sizeof(TYPE_ANGLE)*8;
    string angle = to_string(stoi(data.substr(offset, sizeof(TYPE_ANGLE)*8), nullptr, 2));

    offset -= sizeof(TYPE_POS)*8;
    fi.l = stol(data.substr(offset, sizeof(TYPE_POS)*8), nullptr, 2);
    string posy = to_string(fi.f);

    offset -= sizeof(TYPE_POS)*8;
    fi.l = stol(data.substr(offset, sizeof(TYPE_POS)*8), nullptr, 2);
    string posx = to_string(fi.f);

    offset -= sizeof(TYPE_VARIANTE)*8;
    string variante = to_string(stoi(data.substr(offset, sizeof(TYPE_VARIANTE)*8), nullptr, 2));

    string id;
    if(action == UPDATE) {
        offset -= sizeof(TYPE_ID)*8;
        id = to_string(stoi(data.substr(offset, sizeof(TYPE_ID)*8), nullptr, 2));
    }

    string ret = to_string(FURNITURE) + "{";
    if(action == UPDATE) ret.append(id + ",");
    return ret.append(variante + "," + posx + "," + posy + "," + angle + "}");
}


string Encodeur::DecodeCamera(string data) {

    Flint fi;
    char offset = data.length() - 5;

    offset -= sizeof(TYPE_ZOOM)*8;
    string zoom = to_string(stoi(data.substr(offset, sizeof(TYPE_ZOOM)*8), nullptr, 2));

    offset -= sizeof(TYPE_POS)*8;
    fi.l = stol(data.substr(offset, sizeof(TYPE_POS)*8), nullptr, 2);
    string posy = to_string(fi.f);

    offset -= sizeof(TYPE_POS)*8;
    fi.l = stol(data.substr(offset, sizeof(TYPE_POS)*8), nullptr, 2);
    string posx = to_string(fi.f);

    return to_string(CAMERA) + "{" + posx + "," + posy + "," + zoom + "}";
}


string Encodeur::DecodeCarWithPath(string data) {

    Flint fi;
    char offset = data.length() - 5;

    offset -= sizeof(TYPE_ID)*8;
    string id_finish = to_string(stoi(data.substr(offset, sizeof(TYPE_ID)*8), nullptr, 2));

    offset -= sizeof(TYPE_ID)*8;
    string id_start = to_string(stoi(data.substr(offset, sizeof(TYPE_ID)*8), nullptr, 2));


    return to_string(CAR_WITH_PATH) + "{" + id_start + "," + id_finish + "}";
}