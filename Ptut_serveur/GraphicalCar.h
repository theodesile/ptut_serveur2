#pragma once
#ifndef GRAPHICAL_CAR
#define GRAPHICAL_CAR
#include "Car.h"
#include <SFML/Graphics.hpp>
/*
Représentation graphique d'une voiture
*/
class GraphicalCar : public sf::RectangleShape
{
public :
	GraphicalCar(Car* car);
	//permet de bouger la voiture
	void move();
private:
	Car* m_car;
};

#endif