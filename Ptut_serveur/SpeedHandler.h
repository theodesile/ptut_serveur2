#pragma once
#ifndef SPEEDHANDLER_H
#define SPEEDHANDLER_H
#include <vector>
#include "Point.h"
#include "Car.h"
using namespace std;
class Graph;
/*
Classe fonctionnelle qui permet de gerer les priorites et les vitesse des voitures
Stocke la liste des files de voiture allant a un noeud donne
Chaque speed handler concerne un unique noeud destination
*/
class SpeedHandler
{
public:
	SpeedHandler(int node, Graph* graph);
	//permet d'ajouter une voiture dans un file, retourne la file dans laquelle
	//permet d'ajouter une file a un speedHandler
	void addEdge(Edge* edge);
	//retourne la liste des files
	vector<Edge*> getEdges();
	//retourne la vitesse objectifSpeed a laquelle devrai etre la voiture car
	//dans objectifDist distance
	//renvoie true, si on doit freiner a cause d'une voiture
	bool getRemainingDistance(int nodeFrom, Car* car, float* objectifDist, float* objectifSpeed, bool debug);
	int indexPrioriteMax();
	void refreshPrioritedCar();
private:
	Graph* m_graph;
	int m_node;
	Edge* findEdge(int nodeFrom);
	const float SPEED_PER_RADIUS = 40;
	//renvoie true si pls voitures arrivent au meme moment
	bool isInConflict();
	bool is2CarsInConflict(Car* c1, Car* c2);
	bool is2CarsInConflict1(Car* c1, Car* c2);
	vector<Edge*> lEdges;
	int intersectsegment(Point A, Point B, Point I, Point P);
	Car* prioritedCar;
};
#endif // !SPEEDHANDLER_H

