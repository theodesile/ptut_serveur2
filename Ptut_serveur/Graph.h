#ifndef GRAPH_H
#define GRAPH_H
#include "Edge.h"
#include <vector>
#include "SpeedHandler.h"
/*
Classe pour mod�liser un graph adapte au probleme de route
*/
class Car;
class Graph
{
public:
	Graph();
	~Graph();
	//ajouter un noeud
	void addNode(int id, bool isDepot);
	/*ajouter une arr�te
	*/
	void addEdge(int node1, int node2, int weight, float x, float y, float length, float radius, float angle, float scale = 1);
	//afficher le graph sur la sortie standard
	void printGraph();
	//obtenir la liste des arr�tes d'un noeud i
	std::vector<Edge*>* getNeightboor(int i);
	//obtenir la liste des noeuds
	std::vector<int> getNodes();
	/*algorithme dijkstra, retourne la liste des distances par rapport au noeud source
	le param�tre pred renvoie la liste des noeuds pr�c�dents
	*/
	std::vector<int> dijkstra(int src, std::vector<int>* pred, Edge* forbidenEdge);
	//obtenir l'arr�te du noeud 1 et 2
	Edge* getEdge(int node1, int node2);
	//permet d'appeler la methode getRemaining distance du speed handler de nodeTo
	bool getRemainingDistance(int nodeFrom, int nodeTo, Car* car, float* objectifDist, float* objectifSpeed, bool debug);

	//retourne la liste des depots de voiture
	std::vector<int> getlDepot();
	//retourne le speedhandler correspondant a index
	SpeedHandler* getSpeedHandler(int index);

	//retourne la liste des elements
	std::vector<std::vector<Edge*>> getlElement();

	vector<SpeedHandler*> getLSpeedHandler();
private:
	const float PI = 3.14159265359;
	std::vector<std::vector<Edge*>> m_lElement;

	//contient la liste des noeuds qui peuvent servir de d�pot
	std::vector<int> m_lDepot;
	
	int printSolution(std::vector<int> dist);
	int minDistance(std::vector<int> dist, std::vector<bool> sptSet);
	vector<SpeedHandler*> m_lSpeedHandlers;
};
#endif