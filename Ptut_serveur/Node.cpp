#include "Node.h"

Node::Node(float x, float y, float isDepot)
{
	m_x = x;
	m_y = y;
	isDepot = isDepot;
}

float Node::getX()
{
	return m_x;
}

float Node::getY()
{
	return m_y;
}

bool Node::isDepot()
{
	return m_isDepot;
}
