#include "RoadDescriptor.h"
#include <iostream>
RoadDescriptor::RoadDescriptor(int x, int y, float angle, int length) {
	m_x = x;
	m_y = y;
	m_angle = angle;
	m_length = length;
}
int RoadDescriptor::getX() {
	return m_x;
}
int RoadDescriptor::getLength() {
	return m_length;
}
int RoadDescriptor::getY() {
	return m_y;
}
float RoadDescriptor::getAngle() {
	return m_angle;
}
float RoadDescriptor::getAngleTo(int distance) {
	return m_angle;
}