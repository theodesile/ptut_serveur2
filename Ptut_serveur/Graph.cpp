#include "Graph.h"
#include "Edge.h"
#include <vector>
#include <iostream>
#include <cmath>
#include <climits>
#include "Trigo.h"
#include "SpeedHandler.h"
#include <exception>

Graph::Graph() {
	m_lSpeedHandlers = vector<SpeedHandler*>();
}
void Graph::addNode(int id, bool isDepot) {
	std::vector<Edge*> el;
	m_lElement.push_back(el);
	if (isDepot) {
		m_lDepot.push_back(id);
	}
	m_lSpeedHandlers.push_back(new SpeedHandler(id, this));
}
void Graph::addEdge(int node1, int node2, int weight, float x, float y, float length, float radius, float angle, float scale) {
	x *= scale;
	y *= scale;
	length *= scale;
	float x1 = x + cos(angle + PI / 2) * 12;
	float y1 = y + sin(angle + PI / 2) * 12;
	float x2 = x + cos(angle - PI / 2) * 12;
	float y2 = y + sin(angle - PI / 2) * 12;
	float radius1 =0;
	float radius2 = 0;
	float angle1 = angle;
	float angle2 = angle;
	float l1 = length;
	float l2 = length;
	if (radius != 0) {
		float trueAngle=length/radius;		
		radius1 = radius+ 12;
		radius2 =radius- 12;
		l1 = radius1 * trueAngle;
		l2 = radius2 * trueAngle;
		float angleToAdd = PI / 2 + l2/radius2;
		if (radius2 < 0)
			angleToAdd *= -1;
		x2 -= cos(angle + PI/2) * radius2 + cos(angle + angleToAdd) * radius2;
		y2 -= sin(angle + PI/2) * radius2 + sin(angle + angleToAdd) * radius2;
		angle2-=l2/radius2;		
	}
	else {
		x2 += cos(angle) * length;
		y2 +=sin(angle) * length;
	}
	Edge* e1 = new Edge(node1-1, node2 - 1, weight, x1, y1, l1, radius1, angle);
	m_lElement[node1 - 1].push_back(e1);
	Edge* e2;
	if (radius != 0) {
		e2 = new Edge(node2-1, node1 - 1, weight, x2, y2, l2, radius2, getInIntervalPI(angle2 + PI));
		m_lElement[node2 - 1].push_back(e2);
	}
	else {
		e2 = new Edge(node2 - 1, node1 - 1, weight, x2, y2, l2, radius2, getInIntervalPI(angle2 + PI));
		m_lElement[node2 - 1].push_back(e2);
	}
	m_lSpeedHandlers.at(node2 - 1)->addEdge(e1);
	m_lSpeedHandlers.at(node1 - 1)->addEdge(e2);
} 
Graph::~Graph(){
}
std::vector<int> Graph::getlDepot() {
	return m_lDepot;
}
SpeedHandler* Graph::getSpeedHandler(int index)
{
	return m_lSpeedHandlers.at(index-1);
}
void Graph::printGraph() {
	for (int i = 0; i < m_lElement.size(); i++) {
		for (int a = 0; a < m_lElement[i].size(); a++) {
			std::cout << i << ": " << m_lElement[i][a]->getNodeFrom() << "(" << m_lElement[i][a]->getWeight() <<") ";
		}
		std::cout << "\n";

	}
}
std::vector<int> Graph::getNodes() {
	std::vector<int> retour;
	for (int i = 1; i <= m_lElement.size(); i++) {
		retour.push_back(i);
	}
	return retour;
}
std::vector<Edge*>* Graph::getNeightboor(int i) {
	return &m_lElement[i];
}
int Graph::minDistance(std::vector<int> dist, std::vector<bool> sptSet)
{
	// Initialize min value 
	int min = INT_MAX, min_index;

	for (int v = 0; v < dist.size(); v++)
		if (sptSet[v] == false && dist[v] <= min)
			min = dist[v], min_index = v;

	return min_index;
}
int Graph::printSolution(std::vector<int> dist)
{
	printf("Vertex \t\t Distance from Source\n");
	for (int i = 0; i < dist.size(); i++)
		printf("%d \t\t %d\n", i, dist[i]);
	return 1;
}

Edge* Graph::getEdge(int node1, int node2) {
	for (int i = 0; i < m_lElement[node1-1].size(); i++) {
		if (m_lElement[node1-1][i]->getNodeTo() == node2-1) {
			return m_lElement[node1-1][i];
		}
	}
	return NULL;
}

bool Graph::getRemainingDistance(int nodeFrom, int nodeTo, Car* car, float* objectifDist, float* objectifSpeed, bool debug)
{
	if(nodeTo>=1)
		return m_lSpeedHandlers.at(nodeTo-1)->getRemainingDistance(nodeFrom, car, objectifDist, objectifSpeed, debug);
}


bool notTheSameEdge(Edge* e1, Edge* e2) {
	if (e1 == NULL || e2 == NULL)
		return true;

	return !(e1->getX() == e2->getX() && e1->getY() == e2->getY() &&
		e1->getLength() == e2->getLength() && e1->getAngle() == e2->getAngle());
}
std::vector<int> Graph::dijkstra(int src, std::vector<int>* pred, Edge* forbidenEdge)
{
	if (src > getNodes().size() || src < 0) {
		cout << "impossible de calculer le dijkstra avec la source " << src << endl; return vector<int>();
	}
	Graph* graph = this;
	std::vector<int> dist; // The output array.  dist[i] will hold the shortest 
	// distance from src to i 

	std::vector<bool> sptSet; // sptSet[i] will be true if vertex i is included in shortest 
	// path tree or shortest distance from src to i is finalized 

	// Initialize all distances as INFINITE and stpSet[] as false 
	int size = graph->getNodes().size();
	for (int i = 0; i < size; i++) {
		pred->push_back(-10);
		dist.push_back(INT_MAX);
		sptSet.push_back(false);
	}
	pred->at(src) = 0;

	// Distance of source vertex from itself is always 0 
	dist[src] = 0;
	int predNode = src;

	// Find shortest path for all vertices 
	for (int count = 0; count < size-1; count++) {
		// Pick the minimum distance vertex from the set of vertices not 
		// yet processed. u is always equal to src in the first iteration.
		int u = minDistance(dist, sptSet); 
		
		// Mark the picked vertex as processed 
		sptSet[u] = true;

		// Update dist value of the adjacent vertices of the picked vertex.
		std::vector<Edge*> lNeightboor = *graph->getNeightboor(u);
		for (int v = 0; v < lNeightboor.size(); v++) {
			int neightboor = lNeightboor[v]->getNodeTo();
			// Update dist[v] only if is not in sptSet, there is an edge from 
			// u to v, and total weight of path from src to  v through u is 
			// smaller than current value of dist[v] 
			if (notTheSameEdge(lNeightboor[v], forbidenEdge) &&!sptSet[neightboor] && lNeightboor[v]->getWeight() /*&& dist[u] != INT_MAX*/
				&& dist[u] + lNeightboor[v]->getWeight() < dist[neightboor]) {
				dist[neightboor] = dist[u] + lNeightboor[v]->getWeight();
				pred->at(neightboor) = u;
			}
		}
	}
	// print the constructed distance array 
	return dist;
}

std::vector<std::vector<Edge*>> Graph::getlElement()
{
	return m_lElement;
}


vector<SpeedHandler*> Graph::getLSpeedHandler()
{
	return m_lSpeedHandlers;
}

