#ifndef TRIGO_H
#define TRIGO_H
class Edge;
class Point;
/*
mini lib avec des utilitaires de trigo
*/
const float PI = 3.14159265359;
float calcOrientedAngle(float angle1, float angle2);
float getInIntervalPI(float angle);
float longueurSegment(float x1, float y1, float x2, float y2);
float longueurSegment(Point p1, Point p2);
float getRadiusToCross(Edge e1, Edge e2, double *distanceBefore, Point* p1, Point* p2);

#endif