#ifndef SERVER_H
#define SERVER_H
#include "Car.h"
#include "Edge.h"
#include "Graph.h"
#include <vector>
#include "Application.h"
using namespace std;
/*
classe qui gere les echanges avec le client et la boucle principale de l'app
*/
class Server
{
public:
	Server(Application* application);
	/*
	boucle principale du programme
	fais avancer les voitures et gere les requetes clients
	*/
	void mainloop();
	/*
	selectionne les voitures en partant de l'origine (x0, y0)
	jusque (x0+fenX, y0+fenY)
	*/
	void selectCars(int x0, int y0, int fenX, int fenY, vector<Car*>* lCars);
	/*
	selectionne les routes (edges) en partant de l'origine (x0, y0)
	jusque (x0+fenX, y0+fenY)
	*/
	void selectEdges(int x0, int y0, int fenX, int fenY, vector<Edge>* lEdges);
private:
	Graph* m_graph;
	Application* m_application;
	
};

#endif