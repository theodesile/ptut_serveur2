#include "Application.h"
#include <vector>
#include "cmath"
#include <iostream>
#include "Graph.h"
#include <algorithm>
#include "SpeedHandler.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>  
#include <exception>
//cooucou
using namespace std;
Application::Application(int limitNbVoitures_) {
	m_graph = new Graph();
	limitNbVoitures = limitNbVoitures_;
}
std::vector<Car>* Application::getCars() {
	return &lCars;
}
void Application::commandCourse(int node1, int node2)
{
	if (lCars.size() >= limitNbVoitures) {
		cout << "trop de voitures, demande de course rejetee\n";
		return;
	}
	int closestDepotFromStart = findClosestDepot(node1);
	int closestDepotFromArrived = findClosestDepot(node2);
	if (closestDepotFromArrived < 0 || closestDepotFromStart <0) {
		cout << "pas de depot trouve pour atteindre la personne\n";
		return;
	}
	Car c(this);
	if (c.start(closestDepotFromStart, node1, node2, closestDepotFromArrived))
		lCars.push_back(c);
	else
		cout << "probleme start\n";
}
void Application::commandRandomCourse()
{
	int nb1, nb2;

	/* initialize random seed: */
	srand(time(NULL));

	do {
		nb1 = rand() % 11 + 1;
		nb2 = rand() % 11 + 1;
	} while (nb1 == nb2);
	commandCourse(nb1, nb2);
}
int Application::findClosestDepot(int node)
{
	if (m_graph->getlDepot().size() == 0) {
		cout << "pas de depot sur le graph\n" << endl;
		return -1;
	}
	vector<int> pred;
	vector<int> dist;
	int min = 9999999;
	int indexMin = m_graph->getlDepot().at(0);
	for (int i = 0; i < m_graph->getlDepot().size(); i++) {
		dist = m_graph->dijkstra(m_graph->getlDepot()[i], &pred, NULL);
		if (dist[node - 1] > 0 && dist[node - 1] < min) {
			min = dist[node - 1];
			indexMin = m_graph->getlDepot().at(i);
		}
		pred.clear();
	}
	return indexMin;
}
void Application::removeCarFromL(Car* c)
{
	vector<Car>::iterator it = find(lCars.begin(), lCars.end(), c);
	if (it != lCars.end()) {
		lCars.erase(it);
	}
	else
		cout << "pas supprime\n";
}
void Application::refreshFiles()
{

}
//methode pour charger le graph
void Application::loadGraph() {
	const float PI = 3.14159;
	const double scaleForBackground = 48.0 / 200.0;
	m_graph->addNode(1, true);
	m_graph->addNode(2, false);
	m_graph->addNode(3, false);
	m_graph->addNode(4, false);
	m_graph->addNode(5, false);
	m_graph->addNode(6, true);
	m_graph->addNode(7, false);
	m_graph->addNode(8, false);
	m_graph->addNode(9, false);
	m_graph->addNode(10, false);
	m_graph->addNode(11, true);
	m_graph->addNode(12, false);
	m_graph->addNode(13, false);
	m_graph->addNode(14, false);
	m_graph->addNode(15, false);
	m_graph->addNode(16, false);
	m_graph->addNode(17, false);
	m_graph->addNode(18, false);
	m_graph->addNode(19, false);
	m_graph->addNode(20, true);
	m_graph->addNode(21, false);
	m_graph->addNode(22, false);
	m_graph->addNode(23, false);
	m_graph->addNode(24, false);
	m_graph->addNode(25, false);
	m_graph->addNode(26, false);
	m_graph->addEdge(1, 2, 1, 1236, 3084, 1089, 0, 0, scaleForBackground);
	m_graph->addEdge(2, 3, 1, 2326, 3084, 1087, 0, 0, scaleForBackground);
	m_graph->addEdge(3, 4, 1, 3413, 3084, 491, 0, 7.0*PI/4.0, scaleForBackground);
	m_graph->addEdge(4, 5, 1, 3761, 2735, 497, 0, PI / 4.0, scaleForBackground);
	m_graph->addEdge(5, 6, 1, 4114, 3087, 988, 0, 0, scaleForBackground);
	m_graph->addEdge(7, 8, 1, 3761, 2735, 594, 0, 3.0 * PI / 2.0, scaleForBackground);
	m_graph->addEdge(8, 9, 1, 3761, 2141, 588, 0, 0, scaleForBackground);
	m_graph->addEdge(9, 10, 1, 4350, 2141, 939, 0, 23.0 * PI / 12.0, scaleForBackground);
	m_graph->addEdge(10, 11, 1, 5256, 1898, 521, 0, PI / 12.0, scaleForBackground);
	m_graph->addEdge(10, 12, 1, 5256, 1898, 551, 0, 4.0 * PI / 3.0, scaleForBackground);
	m_graph->addEdge(12, 13, 1, 4981, 1421, 484, 0, 17.0 * PI / 12.0, scaleForBackground);
	m_graph->addEdge(14, 13, 1, 3761, 952, 1094, 0, 0, scaleForBackground);
	m_graph->addEdge(8, 14, 1, 3761, 2141, 1189, 0, 3.0 * PI / 2.0, scaleForBackground);
	m_graph->addEdge(14, 15, 1, 3761, 952, 721, 0, PI, scaleForBackground);
	m_graph->addEdge(15, 16, 1, 3040, 952, 356, 0, PI, scaleForBackground);
	m_graph->addEdge(25, 15, 1, 3040, 2141, 1189, 0, 3.0*PI/2.0, scaleForBackground);
	m_graph->addEdge(16, 17, 1, 2683, 952, 491, 0, 11.0*PI/12.0, scaleForBackground);
	m_graph->addEdge(17, 18, 1, 2209, 1079, 607, 0, PI, scaleForBackground);
	m_graph->addEdge(18, 19, 1, 1603, 1079, 693, 0, PI, scaleForBackground);
	m_graph->addEdge(19, 20, 1, 911, 1079, 593, 0, 3.0*PI/2.0, scaleForBackground);
	m_graph->addEdge(18, 21, 1, 1603, 1079, 694, 0, PI/2.0, scaleForBackground);
	m_graph->addEdge(21, 22, 1, 1603, 1773, 585, 0, 0, scaleForBackground);
	m_graph->addEdge(22, 23, 1, 2188, 1773, 604, 0, PI/3.0, scaleForBackground);
	m_graph->addEdge(23, 24, 1, 2515, 2296, 218, 0, 7.0*PI/4.0, scaleForBackground);
	m_graph->addEdge(24, 25, 1, 2669, 2141, 371, 0, 0, scaleForBackground);
	m_graph->addEdge(25, 8, 1, 3040, 2141, 722, 0, 0, scaleForBackground);
	m_graph->addEdge(26, 23, 1, 2326, 2484, 267, 0, 7.0*PI/4.0, scaleForBackground);
	m_graph->addEdge(2, 26, 1, 2326, 3084, 600, 0, 3.0*PI/2.0, scaleForBackground);

}
void Application::loadCars() {
	for (int i = 0; i < limitNbVoitures / 3;i ++) {
		commandRandomCourse();
	}
}
void Application::moveForward(float d)
{
	if (lCars.size() < 2 * limitNbVoitures / 3) {
		commandRandomCourse();
	}
	Car* c;
	for (int i = 0; i < lCars.size(); i++) {
		c = &lCars[i];
		if (c->moveForward(d)) {
			removeCarFromL(c);
			delete c;
			break;
		}

	}
}
Graph* Application::getGraph() {
	return m_graph;
}