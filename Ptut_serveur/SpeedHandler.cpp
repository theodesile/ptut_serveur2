#include "SpeedHandler.h"
#include "Trigo.h"
#include <iostream>
#include "Graph.h"
#include "cmath"
#include <time.h>  
#include "Point.h"
#include "Car.h"
#include <limits>
#include "Edge.h"
using namespace std;
SpeedHandler::SpeedHandler(int node, Graph* graph)
{
	m_node = node;
	m_graph = graph;
}



void SpeedHandler::addEdge(Edge* edge)
{
	lEdges.push_back(edge);
}
vector<Edge*> SpeedHandler::getEdges()
{
	return lEdges;
}
int SpeedHandler::indexPrioriteMax() {
	size_t max = 0;
	int indexMax = 0;
	for (int i = 0; i < lEdges.size(); i++) {
		//cout << "size  " << i << " = " << lEdges[i]->getCars().size() << "max = " << max << endl;
		if (lEdges[i]->getCars().size() > max) {
			max = lEdges[i]->getCars().size();
			indexMax = i;
			//cout << "affected\n";
		}

	}
	//cout << "index max = " << indexMax << endl;
	return indexMax;
}
void SpeedHandler::refreshPrioritedCar()
{
	if (prioritedCar == NULL) {
		int indexPrioritedEdge = indexPrioriteMax();
		if (lEdges[indexPrioritedEdge]->getCars().size()) {
			prioritedCar = lEdges[indexPrioritedEdge]->getCars()[0];
		}
		return;
	}
	//la voiture peut etre supprimee ici
	if (prioritedCar!= NULL && prioritedCar->getNextNode() != m_node && prioritedCar->getPassedDistance() >= 10) {
		//cout << "refresh\n";
		int indexPrioritedEdge = indexPrioriteMax();
		//cout << "index = " << indexPrioritedEdge << " size de edge " << lEdges[indexPrioritedEdge]->getCars().size() <<  endl;
		if (lEdges[indexPrioritedEdge]->getCars().size()) {
			prioritedCar = lEdges[indexPrioritedEdge]->getCars()[0];
			//cout << "priorited Car next node : " << prioritedCar->getNextNode() << " get current : " << prioritedCar->getCurrentNode() << endl;
		}
	}
}
bool SpeedHandler::getRemainingDistance(int nodeFrom, Car* car, float* distance, float* speed, bool debug)
{
	refreshPrioritedCar();
	Edge* nextEdge = car->getNextEdge(); // tout a refaire
	Edge* currentEdge = car->getCurrentEdge();
	int index = car->getCurrentEdge()->findIndexCar(car);
	int indexCar = car->findIndex();
	if (indexCar > 0) {
		Car* nextCar = currentEdge->getCars()[index-1];
		if (nextCar->getPassedDistance() != 0) {
			if (debug)
				cout << "not first car\n";
			*distance = nextCar->getPassedDistance() - car->getPassedDistance() - 40;
			*speed = 0;
			return true;
		}
	}
	if (isInConflict() && prioritedCar != car) {
			*distance = currentEdge->getLength() - car->getPassedDistance() - 60;
			*speed = 0;
			if (debug) {
				cout << "not priorited car\n";
			}
			return true;
	}
	if (nextEdge!=NULL) {
		SpeedHandler* nextSpeedHandler = m_graph->getSpeedHandler(car->getNextNextNode());
		if (nextEdge->getCars().size() != 0) {
			Car* nextCar = nextEdge->getCars().at(nextEdge->getCars().size() - 1);
			*distance = nextCar->getPassedDistance() - car->getPassedDistance() + currentEdge->getLength() - 50;
			*speed = 0;
			if(debug)
				cout << "car apres le noeud\n";
			return false;
		}
	}
	if (car->getNextNode() == car->getDestination()) {
		if(car->getNextNode()==car->getDestination())
			*distance = currentEdge->getLength() - car->getPassedDistance();
		else
			*distance = currentEdge->getLength() - car->getPassedDistance();
		*speed = 0;
		if (debug)
			cout << "car arrive � destination\n";
		return false;
	}
	if (currentEdge->getRadius() != 0) {
		*distance = currentEdge->getLength() - car->getPassedDistance();
		*speed = SPEED_PER_RADIUS / nextEdge->getRadius();
		if (debug)
			cout << "car apres le noeud\n";
	}
	else {
		if (debug)
			cout << "c ok\n";
		*distance = 100;
		*speed = 2;
	}
	return false;
}

bool SpeedHandler::isInConflict()
{
	if (lEdges.size() <= 2) {
		return false;
	}
	vector<Car*> lCloseCars;
	for (size_t i = 0; i < lEdges.size(); i++) {
		for (size_t j = 0; j < lEdges.size(); j++)
		{
			if (i != j && lEdges[i]->getCars().size() > 0 && lEdges[j]->getCars().size() > 0) {
				if (is2CarsInConflict(lEdges[i]->getCars()[0], lEdges[j]->getCars()[0]));
			}
		}
	}
}
int SpeedHandler::intersectsegment(Point A, Point B, Point I, Point P)
{
	Point D, E;
	D.x = B.x - A.x;
	D.y = B.y - A.y;
	E.x = P.x - I.x;
	E.y = P.y - I.y;
	float t, u;
	double denom = D.x * E.y - D.y * E.x;
	if (denom == 0)
		return -1;   // erreur, cas limite
	t = -(A.x * E.y - I.x * E.y - E.x * A.y + E.x * I.y) / denom;
	if (t < 0 || t >= 1)
		return 0;
	u = -(-D.x * A.y + D.x * I.y + D.y * A.x - D.y * I.x) / denom;
	if (u < 0 || u >= 1)
		return 0;
	return 1;
}
bool SpeedHandler::is2CarsInConflict(Car* c1, Car* c2)
{
	if (c1->getNextNode() != c2->getNextNode())
		return is2CarsInConflict1(c1, c2);
	float dist1, dist2;
	dist1 = c1->getCurrentEdge()->getLength() - c1->getPassedDistance();
	dist2 = c2->getCurrentEdge()->getLength() - c2->getPassedDistance();
	if (dist1 > 90 || dist2 > 90) {
		return false;
	}
	//si l'une des deux arrive a son point d'arrivee cest ok
	if (c1->getNextEdge() == NULL || c2->getNextEdge() == NULL)
		return false;
	//si elle vont au meme endroit pas ok
	if (c1->getNextEdge() == c2->getNextEdge())
		return true;
	//si chacune va a l'endroit ou l'autre se trouve est ok
	if (c1->getNextEdge() == c2->getCurrentEdge() && c1->getCurrentEdge() == c2->getNextEdge()) {
		return false;
	}
	Point A, B, C, D;
	int d = 20 ;
	//on calcule les points de la voiture 1
	A.x = c1->getCurrentEdge()->getX()+ cos(c1->getCurrentEdge()->getAngle()) * (c1->getCurrentEdge()->getLength() - d);
	A.y = c1->getCurrentEdge()->getY() + sin(c1->getCurrentEdge()->getAngle()) * (c1->getCurrentEdge()->getLength() - d);
	B.x = c1->getNextEdge()->getX() + cos(c1->getNextEdge()->getAngle()) * d;
	B.y = c1->getNextEdge()->getY() + sin(c1->getNextEdge()->getAngle()) * d;
	//et de la voiture 2
	C.x = c2->getCurrentEdge()->getX() + cos(c2->getCurrentEdge()->getAngle()) * (c2->getCurrentEdge()->getLength() - d);
	C.y = c2->getCurrentEdge()->getY()+ sin(c2->getCurrentEdge()->getAngle()) * (c2->getCurrentEdge()->getLength() - d);
	D.x = c2->getNextEdge()->getX()+ cos(c2->getNextEdge()->getAngle()) * d;
	D.y = c2->getNextEdge()->getY() +sin(c2->getNextEdge()->getAngle()) * d;
	

	return intersectsegment(A, B, C, D)==1;
}

bool SpeedHandler::is2CarsInConflict1(Car* c1, Car* c2)
{
	if (c1->getNextNextNode() >= 0 && c2->getNextNextNode() >= 0 &&
		c1->getNextNextNode() == c2->getCurrentNode() && c2->getNextNextNode() == c1->getCurrentNode()) {
		return false;
	}
	float angle1, angle2;
	if (c1->getCurrentEdge() != NULL && c1->getNextEdge() != NULL
		&& c2->getCurrentEdge() != NULL && c2->getNextEdge() != NULL) {
		//si les deux voitures vont a l'endroit ou l'autre est
		if (c1->getCurrentNode() == c2->getNextNextNode() &&
			c2->getCurrentNode() == c1->getNextNextNode()) {
			return false;
		}
		//si l'une des deux voitures tourne a droite et qu'elles ne vont pas au 
		//meme endroit
		angle1 = c1->getCurrentEdge()->getAngle() - c1->getNextEdge()->getAngle();
		angle2 = c2->getCurrentEdge()->getAngle() - c2->getNextEdge()->getAngle();
		if ((angle1 < -0.2 || angle2 < -0.2) &&
			(angle1 < PI && angle2 < PI) &&
			c1->getNextEdge() != c2->getNextEdge()
			&& c2->getCurrentNode() != c1->getNextNode()
			&& c1->getCurrentNode() != c2->getNextNode()) {
			return false;
		}
	}
	float dist1 = c1->getCurrentEdge()->getLength() - c1->getPassedDistance();
	float dist2 = c2->getCurrentEdge()->getLength() - c2->getPassedDistance();
	int d1 = 80;//a revoir, degueu
	int d2 = 80;
	float dista1 = dist1 - d2;
	float dista2 = dist2 - d2;
	float distb1 = dist1 + d1;
	float distb2 = dist2 + d1;
	float ta1 = dista1 / c1->getSpeed();
	float ta2 = dista2 / c2->getSpeed();
	float tb1 = distb1 / c1->getSpeed();
	float tb2 = distb2 / c2->getSpeed();
	if ((ta1 >= ta2 && ta1 <= tb2) || (tb2 >= ta1 && tb2 <= tb2)) {
		return true;
	}

	return false;

}

