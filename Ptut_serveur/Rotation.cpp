#include "Rotation.h"
#include <iostream>
#include "Car.h"
#include "Trigo.h"
Rotation::Rotation(Edge* e1, Edge* e2)
{
	edgeDest = e2;
	if (e1 == e2 || calcOrientedAngle(e1->getAngle(),e2->getAngle())==0) {
		distBeforeRotation = 0;
		angle = 0;
		radius = 0;
	}
	else if (e1->getNodeFrom() == e2->getNodeTo()) {
		distBeforeRotation = 15;
		radius = 16;
		angle = -PI;
	}
	else {
		angle = calcOrientedAngle(e1->getAngle(), e2->getAngle());
		if (angle < 0) { //<0 = gauche
			p1 = e1->getPointFromEnd(10);
			p2 = e2->getPointFromStart(0);
			radius = getRadiusToCross(*e1, *e2, &distBeforeRotation, &p1, &p2);
			if (abs(angle) < 0.3)
				distBeforeRotation = 0;
			distBeforeRotation += 24;
		}
		else if (angle > 0) {//droite
			distBeforeRotation = 15;
			p1 = e1->getPointFromEnd(15);
			p2 = e2->getPointFromStart(15);
			radius = getRadiusToCross(*e1, *e2, &distBeforeRotation, &p1, &p2);
			if (abs(angle) < 0.3)
				distBeforeRotation = 0;
		}
		angle = getInIntervalPI(angle);
	}
}

bool Rotation::makeRotation(Car* c)
{
	if (radius == 0)
		return true;

	float angleFinal = angle;
	if (angle > 0)
		angleFinal -= PI / 2;
	else
		angleFinal += PI/2;

	if (calcOrientedAngle(edgeDest->getAngle(), c->getAngle()) > -0.1 && calcOrientedAngle(edgeDest->getAngle(), c->getAngle()) < 0.1) {
		c->setAngle(edgeDest->getAngle());
		return true;
	}
	float length = radius * abs(angle);
	float dangle = (angle * c->getSpeed()) / length;
	if (angle < 0) {
		c->addToAngle(dangle);
	}
	else {
		c->addToAngle(dangle);
	}
	return false;
}