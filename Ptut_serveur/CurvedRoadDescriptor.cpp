#include "CurvedRoadDescriptor.h"
#include "RoadDescriptor.h"
#include <iostream>

CurvedRoadDescriptor::CurvedRoadDescriptor(int x, int y, float angle, int length, int radius):
	RoadDescriptor(x, y, angle, length)
{
	m_radius = radius;
}
float CurvedRoadDescriptor::getAngleTo(int distance) { 
	//attention formule � tester
	return getAngle() + getLength() / distance / getRadius();
}

int CurvedRoadDescriptor::getRadius() {
	return m_radius;
}