#ifndef COURSEMANAGER_H
#define COURSEMANAGER_H
#include "Graph.h"
#include <vector>
#include "Car.h"

using namespace std;
class CourseManager
{
public:
	CourseManager(std::vector<Car*>* lCar, Graph* graph);
	bool askForCourse(int from, int to);
	void run();
private:
	Graph* m_graph;
	vector<Car*>* m_lCar;
	int findClosestDepot(int from);
};

#endif