#include "Trigo.h"
#include <iostream>
#include <math.h>
#include "Edge.h"
#include "Point.h"
using namespace std;

float calcOrientedAngle(float angle1, float angle2) {
	angle1 = getInIntervalPI(angle1);
	angle2 = getInIntervalPI(angle2);
	float dif1 = angle2 - angle1; 
	dif1 = getInIntervalPI(dif1);
	return dif1;
}
float getInIntervalPI(float angle) {
	while (angle < -PI) {
		angle += 2 * PI;
	}
	while (angle > PI) {
		angle -= 2 * PI;
	}
	return angle;
}
float longueurSegment(float x1, float y1, float x2, float y2) {
	return sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2));
}

float longueurSegment(Point p1, Point p2)
{
	return longueurSegment(p1.x, p1.y, p2.x, p2.y);
}

float getRadiusToCross(Edge e1, Edge e2, double* distanceBefore, Point* p1, Point* p2)
{
	double dist = 0;
	do {
		dist += 1;
		*p1 = e1.getPointFromEnd(dist);
		*p2 = e2.getPointFromStart(dist);
	} while (longueurSegment(*p1, *p2) < 20);
	*distanceBefore = dist;
	float angleToCros = calcOrientedAngle(e1.getAngle(), e2.getAngle());
	if (angleToCros == 0)
		return 0.f;
	float angleDroit1, angleDroit2;
	if (angleToCros < 0) {
		angleDroit1 =getInIntervalPI (e1.getAngle() + PI/2);
		angleDroit2 =getInIntervalPI( e2.getAngle() - PI / 2);
	}
	else {
		angleDroit1 = getInIntervalPI(e1.getAngle() - PI / 2);
		angleDroit2 = getInIntervalPI(e2.getAngle() + PI / 2);
	}
	const float longueurBis = 10;
	Point p1B, p2B;
	p1B.x = p1->x + cos(angleDroit1) * longueurBis;
	p1B.y = p1->y + sin(angleDroit1) * longueurBis;
	p2B.x = p2->x + cos(angleDroit2) * longueurBis;
	p2B.y = p2->y + sin(angleDroit2) * longueurBis;
	
	float dist1 = longueurSegment(p1->x, p1->y, p2->x, p2->y);
	float dist2 = longueurSegment(p1B.x, p1B.y, p2B.x, p2B.y);

	float radius = (-longueurBis * dist1) / (dist2 - dist1);
	cout << "dist 1 = " << dist1 << endl;
	cout << "dist 2 = " << dist2 << endl;
	cout << "radius = " << radius << endl;
	return abs(radius);
}
