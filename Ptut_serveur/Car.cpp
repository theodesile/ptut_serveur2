#include "Car.h"
#include "Edge.h"
#include <iostream>
#include "Application.h"
#include "Graph.h"
#include "Trigo.h"
#include "Rotation.h"
Car::Car(Application* application_)
{
	application = application_;
	graph = application->getGraph();
}

bool Car::start(int beginNode_, int peopleNode_, int nodeObj_, int nodeDest_)
{
	if (peopleNode_ == nodeObj_) {
		cout << "people node == nodeobj\n";
		return false;
	}

	beginNode = beginNode_;
	peopleNode = peopleNode_;
	nodeObj = nodeObj_;
	nodeDest = nodeDest_;
	speed = 2;
	acceleration = 0;
	if (!calcItineraire()){
		return false;
	}
	calcDistBeforeRotation();
	lNextEdges[0]->addCar(this);
	x = lNextEdges[0]->getX();
	y = lNextEdges[0]->getY();
	angle = lNextEdges[0]->getAngle();
	passedDistance = 0;
	return true;
}

float Car::getPassedDistance()
{
	return passedDistance;
}

bool Car::moveForward(float dtime)
{
	bool b = handleAcceleration();
	speed += dtime * acceleration;
	//cout << "x : " << x << " y : " << y << " angle : " << angle << " passed distance = " << passedDistance << " dist before rotation = " << lRotation[indexIt].distBeforeRotation;
	//cout << " edge length = " << lNextEdges[indexIt]->getLength() << endl;
	if (speed > 0) {
		x += cos(angle) * speed*dtime;
		y += sin(angle) * speed*dtime;
		passedDistance += speed*dtime;
	}
	if (indexIt<maxIndexIt - 1 && passedDistance >= lNextEdges[indexIt]->getLength()) {
		lNextEdges[indexIt]->removeCar(this);
		indexIt += 1;
		lNextEdges[indexIt]->addCar(this);
		x = lNextEdges[indexIt]->getX();
		y = lNextEdges[indexIt]->getY();
		angle = lNextEdges[indexIt]->getAngle();
		passedDistance = 0;
	}
	if(indexIt>=maxIndexIt-1 && passedDistance + lRotation[indexIt].distBeforeRotation > lNextEdges[indexIt]->getLength()) {
		cout << "arrivee \n";
		lNextEdges[indexIt]->removeCar(this);
		return true;
	}
	return false;
}
void Car::calcDistBeforeRotation() {
	Edge* e1;
	Edge* e2;
	for (int i = 0; i < lNextEdges.size() - 1; i++) {
		e1 = lNextEdges[i];
		e2 = lNextEdges[i + 1];
		lRotation.push_back(Rotation(e1, e2));
	}
}
void Car::setPassedDistance(float d)
{
	passedDistance = d;
}
void Car::addToAngle(float f)
{
	angle += f;
}
void Car::setAngle(float angle_)
{
	angle = angle_;
}
bool Car::calcItineraire()
{
	if (beginNode!=peopleNode && !addNodeToIt(beginNode, peopleNode, true)) return false;
	if (peopleNode != nodeObj && !addNodeToIt(peopleNode, nodeObj, true)) return false;
	if (nodeObj != nodeDest && !addNodeToIt(nodeObj, nodeDest, true)) return false;
	itineraire.push_back(nodeDest);
	int node1, node2;
	Edge* e;
	for (int i = 0; i < itineraire.size() - 1; i++) {
		node1 = itineraire[i];
		node2 = itineraire[i+1];
		e = graph->getEdge(node1, node2);
		if (e == NULL) {
			cout << "probleme dans le calcul d'itineraire\n";
		}
		else {
			lNextEdges.push_back(e);
		}
	}
	maxIndexIt = itineraire.size() - 1;
	return true;
}

bool Car::addNodeToIt(int node1, int node2, bool dontAllowReturn)
{
	vector<int> lpredecesseur;
	Edge* forbidenEdge = NULL;
	int sizeIt = itineraire.size();
	if (sizeIt >= 2 && dontAllowReturn) {
		forbidenEdge = graph->getEdge(itineraire[sizeIt - 1], itineraire[sizeIt - 2]);
	}
	graph->dijkstra(node1-1, &lpredecesseur, forbidenEdge);
	vector<int> reverseIt = vector<int>();
	int currentNode = node2 - 1;
	while (currentNode >= 0 && currentNode != node1-1) {
		currentNode = lpredecesseur[currentNode]; 
		reverseIt.push_back(currentNode + 1);
	}
	if (currentNode < 0) {
		if(dontAllowReturn)
			return addNodeToIt(node1, node2, false);
		return false;
	}
	for (int i = reverseIt.size() - 1; i >= 0; i--) {
		itineraire.push_back(reverseIt[i]);
	}
	return true;
}

bool Car::handleAcceleration()
{

	float objectifSpeed;
	float objectifDist;
	bool isCar = graph->getRemainingDistance(itineraire[indexIt], itineraire[indexIt+1], this, &objectifDist, &objectifSpeed, b);
	float necessaryDist = abs(10 * (speed - objectifSpeed) * speed);
	if (necessaryDist >= objectifDist && speed >= 0 && speed > objectifSpeed) {
		acceleration = -0.1;
	}
	else if (speed < 6) {
		acceleration = 0.1;
	}
	else {
		acceleration = 0;
	}
	return isCar;
}

Edge* Car::getCurrentEdge()
{
	return lNextEdges[indexIt];
}

Edge* Car::getNextEdge()
{
	if (indexIt + 1 < lNextEdges.size())
		return lNextEdges[indexIt + 1];
	return NULL;
}

int Car::getCurrentNode()
{
	return itineraire[indexIt];

}

int Car::getNextNode()
{
	if (indexIt + 1 < itineraire.size())
		return itineraire[indexIt + 1];
	return -1;
}

int Car::getNextNextNode()
{
	if (indexIt + 2 < itineraire.size())
		return itineraire[indexIt + 2];
	return -1;
}

int Car::getDestination()
{
	return nodeDest;
}

int Car::findIndex()
{
	return lNextEdges[indexIt]->findIndexCar(this);
}

float Car::getSpeed()
{
	return speed;
}

float Car::getX()
{
	return x;
}

float Car::getY()
{
	return y;
}

float Car::getAngle()
{
	return angle;
}

vector<int> Car::getItineraire()
{
	return itineraire;
}

vector<Rotation> Car::getRotations()
{
	return lRotation;
}
