#pragma once
#include "RoadDescriptor.h"
/*
Classe fille de RoadDescritor, permet de représenter une route droite
*/
class StraightRoadDescriptor : public RoadDescriptor
{
public:
	StraightRoadDescriptor(int x, int y, float angle, int length);
	float getAngleTo(int distance);
};