
#include "Edge.h"
#include <vector>
#include <iostream>
#include "Car.h"
#include <cmath>
using namespace std;

Edge::Edge()
{
}



Edge::Edge(int node1, int node2, float weight, float x, float y, float length, float radius, float angle)
{
	lCars.empty();
	m_nodeFrom = node1;
	m_nodeTO = node2;
	m_weight = 0.1;
	nbCars = 0;
	m_x = x;
	m_y = y;
	m_length = length;
	m_radius = radius;
	m_angle = angle;

	m_id = newidroad += 2;
}
int Edge::getNodeFrom() {
	return m_nodeFrom;
}
int Edge::getNodeTo()
{
	return m_nodeTO;
}
float Edge::getWeight() {
	return m_weight;
}

float Edge::getX()
{
	return m_x;
}

float Edge::getY()
{
	return m_y;
}

float Edge::getAngle()
{
	return m_angle;
}

float Edge::getLength()
{
	return m_length;
}

float Edge::getRadius()
{
	return m_radius;
}

int Edge::findIndexCar(Car* c)
{
	for (int i = 0; i < lCars.size(); i++) {
		if (lCars[i] == c)
			return i;
	}
	return -1;
}

void Edge::addCar(Car* c)
{
	if(findIndexCar(c)==-1)
		lCars.push_back(c);
	m_weight = (lCars.size() / m_length) * 100;
}

void Edge::removeCar(Car* c)
{
	for (auto i = lCars.begin(); i != lCars.end(); i++) {
		if (*i == c) {
			lCars.erase(i);
			return;
		}
	}
	m_weight = (lCars.size() / m_length) * 100;
	cout << "car pas supprimee de Edge\n";
}

int Edge::getMId() const {
    return m_id;
}

Point Edge::getPointFromEnd(float d)
{
	float dist = m_length - d;
	Point p;
	p.x = cos(m_angle) * dist + m_x;
	p.y = sin(m_angle ) * dist + m_y;
	return p;
}

Point Edge::getPointFromStart(float d)
{
	Point p;
	p.x = cos(m_angle) * d + m_x;
	p.y = sin(m_angle) * d + m_y;
	return p;
}

void Edge::setMId(int mId) {
    m_id = mId;
}

vector<Car*> Edge::getCars()
{
	return lCars;
}
