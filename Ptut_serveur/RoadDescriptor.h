#ifndef ROADESCRIPTOR
#define ROADESCRIPTOR

/*
Classe qui permet de mod�liser de repr�senter une route physique
*/
class RoadDescriptor {
public:
	RoadDescriptor(int x, int y, float angle, int length);
	int getX();
	int getY();
	virtual float getAngle();
	int getLength();
	//retourne l'angle de la route � la distance distance
	virtual float getAngleTo(int distance);
private:
	int m_x, m_y, m_length;
	float m_angle;
};
#endif // !ROADESCRIPTOR