#ifndef EDGE
#define EDGE
#include "Point.h"
#include <vector>
using namespace std;
class Car;
/*
Classe qui permet de mod�liser une arr�te de graph
*/
class Edge
{
public:
	Edge();
	Edge(int node1, int node2, float weight, float x, float y, float length, float radius, float angle);
	int getNodeFrom();
	int getNodeTo();
	float getWeight();
	float getX();
	float getY();
	float getAngle();
	float getLength();
	float getRadius();
	int findIndexCar(Car* c);
	void addCar(Car* c);
	void removeCar(Car* c);
    int getMId() const;
	Point getPointFromEnd(float d);
	Point getPointFromStart(float d);
    void setMId(int mId);
    static int newidroad;
	Point p1, p2;
	vector<Car*> getCars();
private:
    float m_x, m_y, m_angle, m_length, m_radius;
    int m_nodeFrom;
	int m_nodeTO;
    float m_weight;
    int m_id;
	vector<Car*> lCars;
	int nbCars;
};
#endif // !1