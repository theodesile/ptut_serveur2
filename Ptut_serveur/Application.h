#pragma once
#ifndef APPLICATION
#define APPLICATION
#include "Graph.h"
#include "Car.h"
using namespace std;

/*
Classe centrale de ce projet, g�re les graphs, les routes, les voitures
Permet de centraliser toutes les informations
*/
class Application
{
public:
	Application(int limitNbVoitures_);
	Graph* getGraph();
	/*
	permet de charger un graph d'exemple
	*/
	void refreshFiles();
	void loadGraph();
	/*
	permet de charger un graph d'exemple
	*/
	void loadCars();
	//methode appellee pour faire avancer toutes les voitures
	void moveForward(float d);
	/*methode pour commander une course a une voiture, fait tous les calculs, trouve le depot le plus proche,etc...
	*/
	void commandCourse(int node1, int node2);
	void commandRandomCourse();

	std::vector<Car>* getCars();
private:
	/*permet de trouver le depot le plus proche
	*/
	int findClosestDepot(int node);
	/*permet de supprimer une voiture de la liste quand celle-ci a fini son trajet
	*/
	void removeCarFromL(Car* c);
	Graph* m_graph;
	std::vector<Car> lCars;
	int limitNbVoitures;

	
};

#endif