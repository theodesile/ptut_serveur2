/**
 * @file CPPtoJS.h
 * @brief Compacter les messages de communication entre le serveur et le client.
 * @author Etienne.T
 * @version 0.1
 * @date 14 septembre 2019
 *
 * Pas encore de description détaillé.
 *
 */

#ifndef CPPTOJS_ENCODEUR_H
#define CPPTOJS_ENCODEUR_H

#include <iostream>
#include <string>
#include <bitset>

using namespace std;


// DEFINITION DES TYPES = = = = = = = = = = = = = = = =

typedef unsigned short TYPE_ID;
typedef float TYPE_POS;
typedef short TYPE_ANGLE;
typedef short TYPE_LONGUEUR;
typedef short TYPE_RAYON;
typedef char  TYPE_VITESSE;
typedef char  TYPE_VARIANTE;
typedef short TYPE_ZOOM;
typedef bool  TYPE_SENS_UNIQUE;

//= = = = = = = = = = = = = = = = = = = = = = = = = = =

class Encodeur {

public:

    /**
     * Constantes servant à identifier l'action à réaliser
     */
    enum Action : unsigned char {
        ADD    = 0b00,
        DELETE = 0b01,
        UPDATE = 0b10
    };

    /**
     * Constantes servant à identifier le type d'objet sur lequel on travaille
     */
    enum CityObject : unsigned char {
        CAMERA        = 0b000,
        STRAIGHT_ROAD = 0b001,
        CURVED_ROAD   = 0b010,
        CAR           = 0b011,
        PEDESTRIAN    = 0b100,
        FURNITURE     = 0b101,
        CAR_WITH_PATH = 0b110
    };

    /**
     * Encode l'info "supprimer un objet"
     *
     * @param id Identifiant de l'objet à supprimer
     *
     * @return string de l'info encodée
     */
    static string Delete(TYPE_ID id);

    /**
     * Encode l'info "ajouter une route droite"
     *
     * @param posx Position X de l'objet à créer
     * @param posy Position Y de l'objet à créer
     * @param angle Angle de rotation de l'objet à créer
     * @param longueur Longueur de l'objet à créer
     *
     * @return string représentant l'info encodée
     */
    static string AddRoad(TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_LONGUEUR longueur, TYPE_SENS_UNIQUE sens_unique);

    /**
     * Encode l'info "modifier une route droite"
     *
     * @param id Identifiant d'instance de l'objet à modifier
     * @param posx Position X de l'objet à modifier
     * @param posy Position Y de l'objet à modifier
     * @param angle Angle de rotation de l'objet à modifier
     * @param longueur Longueur de l'objet à modifier
     *
     * @return string représentant l'info encodée
     */
    static string UpdateRoad(TYPE_ID id, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_LONGUEUR longueur, TYPE_SENS_UNIQUE sens_unique);

    /**
     * Encode l'info "ajouter une route courbe"
     *
     * @param posx Position X de l'objet à créer
     * @param posy Position Y de l'objet à créer
     * @param angle Angle de rotation de l'objet à créer
     * @param longueur Longueur de l'objet à créer
     * @param rayon Rayon de la courbe de la route à créer
     *
     * @return string représentant l'info encodée
     */
    static string AddRoad(TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_LONGUEUR longueur, TYPE_RAYON rayon, TYPE_SENS_UNIQUE sens_unique);

    /**
     * Encode l'info "modifier une route courbe"
     *
     * @param id Identifiant d'instance de l'objet à modifier
     * @param posx Position X de l'objet à modifier
     * @param posy Position Y de l'objet à modifier
     * @param angle Angle de rotation de l'objet à modifier
     * @param longueur Longueur de l'objet à modifier
     * @param rayon Rayon de la courbe de la route à modifier
     *
     * @return string représentant l'info encodée
     */
    static string UpdateRoad(TYPE_ID id, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_LONGUEUR longueur, TYPE_RAYON rayon, TYPE_SENS_UNIQUE sens_unique);

    /**
     * Encode l'info "ajouter une voiture"
     *
     * @param variante Variante de voiture à créer
     * @param posx Position X de l'objet à créer
     * @param posy Position Y de l'objet à créer
     * @param angle Angle de rotation de l'objet à créer
     * @param vitesse Vitesse de la voiture à créer
     *
     * @return string représentant l'info encodée
     */
    static string AddCar(TYPE_VARIANTE variante, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_VITESSE vitesse);

    /**
     * Encode l'info "modifier une voiture"
     *
     * @param id Identifiant d'instance de l'objet à modifier
     * @param variante Variante de voiture à modifier
     * @param posx Position X de l'objet à modifier
     * @param posy Position Y de l'objet à modifier
     * @param angle Angle de rotation de l'objet à modifier
     * @param vitesse Vitesse de la voiture à modifier
     *
     * @return string représentant l'info encodée
     */
    static string UpdateCar(TYPE_ID id, TYPE_VARIANTE variante, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_VITESSE vitesse);

    /**
     * Encode l'info "ajouter un piéton"
     *
     * @param variante Variante du piéton à créer
     * @param posx Position X de l'objet à créer
     * @param posy Position Y de l'objet à créer
     * @param angle Angle de rotation de l'objet à créer
     * @param vitesse Vitesse de la voiture à créer
     *
     * @return string représentant l'info encodée
     */
    static string AddPedestrian(TYPE_VARIANTE variante, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_VITESSE vitesse);

    /**
     * Encode l'info "modifier un piéton"
     *
     * @param id Identifiant d'instance de l'objet à modifier
     * @param variante Variante du piétons à modifier
     * @param posx Position X de l'objet à modifier
     * @param posy Position Y de l'objet à modifier
     * @param angle Angle de rotation de l'objet à modifier
     * @param vitesse Vitesse de la voiture à modifier
     *
     * @return string représentant l'info encodée
     */
    static string UpdatePedestrian(TYPE_ID id, TYPE_VARIANTE variante, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_VITESSE vitesse);

    /**
     * Encode l'info "ajouter un mobilier urbain"
     *
     * @param variante Type de mobilier urbain a créer
     * @param posx Position X de l'objet à créer
     * @param posy Position Y de l'objet à créer
     * @param angle Angle de rotation de l'objet à créer
     *
     * @return string représentant l'info encodée
     */
    static string AddFurniture(TYPE_VARIANTE variante, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle);

    /**
     * Encode l'info "modifier un mobilier urbain"
     *
     * @param id Identifiant d'instance de l'objet à modifier
     * @param variante Type du mobilier urbain à modifier
     * @param posx Position X de l'objet à modifier
     * @param posy Position Y de l'objet à modifier
     * @param angle Angle de rotation de l'objet à modifier
     *
     * @return string représentant l'info encodée
     */
    static string UpdateFurniture(TYPE_ID id, TYPE_VARIANTE variante, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle);

    /**
     * Decode une info
     *
     * @param chunk string représentant k'info à décoder
     *
     * @return string de l'info décodé
     */
    static string Decode(string chunk);

private:

    Encodeur()= default;

    union Flint {
        int i;
        long l;
        float f;
    };

    static string Compress(string data);
    static string Decompress(string data);

    static string DecodeFurniture(string data);
    static string DecodeCarPedestrian(string data);
    static string DecodeRoad(string data);
    static string DecodeCamera(string data);
    static string DecodeCarWithPath(string data);

    static string EncodeCarPedestrian(Action action, TYPE_ID id, CityObject type, TYPE_VARIANTE variante, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_VITESSE vitesse);
    static string EncodeFurniture(Action action, TYPE_ID id, TYPE_VARIANTE variante, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle);

    static string EncodeRoad(Action action, TYPE_ID id, CityObject type, TYPE_POS posx, TYPE_POS posy, TYPE_ANGLE angle, TYPE_LONGUEUR longueur, TYPE_RAYON rayon,  TYPE_SENS_UNIQUE sens_unique);
};


#endif //CPPTOJS_ENCODEUR_H