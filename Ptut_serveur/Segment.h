#pragma once
class Segment
{
public :
	Segment(int xA_, int xB_, int yA_, int yB_);
	int isIntersect(Segment* s);
	int xA, xB, yA, yB;
	float angle;
	void calcAngle();
};

