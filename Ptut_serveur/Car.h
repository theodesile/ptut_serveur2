#ifndef CAR_H
#define CAR_H
#include "Edge.h"
#include <vector>
#include "Rotation.h"
class Application;
class Graph;

/*
Classe qui permet de modeliser une voiture
*/
class Car
{
public:
	Car(Application* application_);
	//bit de debug
	bool b;
	static int newidcar;
	//fait avancer la voiture, renvoie true si elle est arriv�e
	bool start(int beginNode_, int peopleNode_, int nodeObj_, int nodeDest_);
	//retourne la distance parcourue depuis le dernier noeud
	float getPassedDistance();
	//permet de faire avancer la voiture de dtime
	bool moveForward(float dtime);
	Edge* getCurrentEdge();
	Edge* getNextEdge();
	int getCurrentNode();
	int getNextNode();
	int getNextNextNode();
	int getDestination();
	int findIndex();
	float getSpeed();
	float getX();
	float getY();
	float getAngle();
	vector<int> getItineraire();
	vector<Rotation> getRotations();
	void calcDistBeforeRotation();
	void setPassedDistance(float d);
	void addToAngle(float f);
	void setAngle(float angle_);
private:
	bool calcItineraire();
	bool addNodeToIt(int node1, int node2, bool dontAllowReturn);
	float x, y, speed, acceleration;
	long float angle;
	int beginNode, peopleNode, nodeObj, nodeDest, indexIt, maxIndexIt;
	vector<int> itineraire;
	vector<Edge*> lNextEdges;
	vector<Rotation> lRotation;
	long double passedDistance;
	bool handleAcceleration();
	Graph* graph;
	Application* application;
};
#endif // !CAR_H