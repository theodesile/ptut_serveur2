#include "SpeedHandler.h"
#include "Trigo.h"
#include <iostream>
using namespace std;
SpeedHandler::SpeedHandler(int node)
{
	m_node = node;
	m_lFiles = new vector<File*>;
}

void SpeedHandler::addCar(int nodeFrom, Car* car)
{
	File* file = findFile(nodeFrom);
	if (file == NULL)
		return;
	file->addCar(car);
}

void SpeedHandler::removeCar(int nodeFrom, Car* car)
{
	File* file = findFile(nodeFrom);
	if (file != NULL)
		file->removeCar(car);

}

void SpeedHandler::addFile(int nodeFrom)
{
	m_lFiles->push_back(new File(nodeFrom));
}
int indexPrioriteMax(vector<File*>* file) {
	float max = -100;
	int indexMax = 0;
	for (int i = 0; i < file->size(); i++) {
		if (file->at(i)->getLCars()->size() > 0 && file->at(i)->getPriorite() > max) {
			max = file->at(i)->getPriorite();
			indexMax = i;
		}
	}
	return indexMax;
}
void SpeedHandler::getRemainingDistance(int nodeFrom, Car* car, float* distance, float* speed)
{
	Edge* nextEdge = car->getNextEdge();
	Edge* currentEdge = car->getCurrentEdge();
	if (isInConflict()) {
		int indexMax = indexPrioriteMax(m_lFiles);
		int index = findIndexFile(nodeFrom); //d�gueu
		cout << "conflit\n";
		cout << "index max = " << indexMax << " index = " << index << endl;
		if (index != indexMax) {
			cout << "ralentissement\n";
			*speed = 0;
			*distance = currentEdge->getLength() - car->getPassedDistance() -30;
			return;
		}
	}
	if (nextEdge == NULL) {
		*distance = currentEdge->getLength() - car->getPassedDistance();
		*speed = 0;
		return;
	}
	if (currentEdge->getRadius() != 0) {
		*distance = 0;

		*speed = abs(currentEdge->getRadius()) / SPEED_PER_RADIUS;
		return;
	}
	if (currentEdge->getAngle() != nextEdge->getAngle()) {
		float angleRotation = calcOrientedAngle(currentEdge->getAngle(), nextEdge->getAngle());
		float distBeforeRotation = abs(tan(angleRotation / 2) * 6);
		*distance = currentEdge->getLength() - car->getPassedDistance() - distBeforeRotation;
		*speed = 0.05;
		return;
	}
	if (currentEdge->getRadius() != 0) {
		*distance = currentEdge->getLength() - car->getPassedDistance();
		*speed = SPEED_PER_RADIUS / nextEdge->getRadius();;
	}
	else {
		*distance = 10000;
		*speed = 100000;
	}
}

File* SpeedHandler::findFile(int nodeFrom)
{
	File* file = NULL;
	for (int i = 0; i < m_lFiles->size(); i++) {
		if (m_lFiles->at(i)->getNodeFrom() == nodeFrom) {
			file = m_lFiles->at(i);
			break;
		}
	}
	return file;
}

bool SpeedHanler::isInConflict()
{
	vector<Car*> lCloseCars = vector<Car*>();
	int size;
	for (int i = 0; i < m_lFiles->size(); i++) {
		size = m_lFiles->at(i)->getLCars()->size();
		if (size > 0)
			lCloseCars.push_back(m_lFiles->at(i)->getLCars()->at(0));
	}
	for (int i = 0; i < lCloseCars.size(); i++) {
		cout << "size = " << lCloseCars.size() << endl;
		for (int j = i + 1; j < lCloseCars.size(); j++) {
			if (is2CarsInConflict(lCloseCars.at(i), lCloseCars.at(j)))
				return true;
		}
	}
	return false;
}

bool SpeedHanler::is2CarsInConflict(Car* c1, Car* c2)
{
	if (c1->getSpeed() == 0 || c2->getSpeed() == 0)
		return false;
	float dist1 = c1->getCurrentEdge()->getLength() - c1->getPassedDistance();
	float dist2 = c2->getCurrentEdge()->getLength() - c2->getPassedDistance();
	float dista1 = dist1 - 13;
	float dista2 = dist2 - 13;
	float distb1 = dist1 + 13;
	float distb2 = dist2 + 13;
	float ta1 = dista1 / c1->getSpeed();
	float ta2 = dista2 / c2->getSpeed();
	float tb1 = distb1 / c1->getSpeed();
	float tb2 = distb2 / c2->getSpeed();
	if ((ta1 >= ta2 && ta1 <= tb2) || (tb2 >= ta1 && tb2 <= tb2)) {
		return true;
	}
	
	return false;
}

int SpeedHanler::findIndexFile(int nodeFrom)
{
	for (int i = 0; i < m_lFiles->size(); i++)
		if (m_lFiles->at(i)->getNodeFrom() == nodeFrom)
			return i;
	return -1;
}
