#pragma once
class Node
{
private:
	Node(float x, float y, float isDepot);
	float m_x, m_y;
	bool m_isDepot;//true si depot 
public:
	float getX();
	float getY();
	bool isDepot();

};