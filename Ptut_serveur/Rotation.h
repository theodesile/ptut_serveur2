#ifndef ROTATION_H
#define ROTATION_H
#include"Edge.h"
#include "Point.h"
class Car;
class Rotation
{
private:
	Edge* edgeDest;
public :
	Rotation(Edge* e1, Edge* e2);
	long float angle, distBeforeRotation, radius;
	bool makeRotation(Car* c);
	Point p1, p2;
};

#endif // !ROTATION_H