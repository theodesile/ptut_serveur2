#include "CourseManager.h"
#include <limits>
#include <vector>
#include <iostream>
using namespace std;
CourseManager::CourseManager(std::vector<Car*>* lCar, Graph* graph)
{
	m_lCar = lCar;
	m_graph = graph;
}

int CourseManager::findClosestDepot(int from) {
	cout << "find closest depot\n";
	int min = numeric_limits<int>::max();
	vector<int>* pred= &vector<int>();
	vector<int> result;
	int closestNode = 0;
	for (int i = 0; i < m_graph->getlDepot().size(); i++) {
		pred->clear();
		cout << "a\n";
		result = m_graph->dijkstra(m_graph->getlDepot().at(i), pred);
		cout << "b\n";
		if (result[from - 1] < min) {
			cout << "if\n";
			closestNode = m_graph->getlDepot().at(i);
			min = result[from - 1];
		}
		cout << "i = " << i << " depot = " << m_graph->getlDepot().at(i) << endl;
	}
	return closestNode;
}
bool CourseManager::askForCourse(int from, int to)
{
	cout << "ask for course\n";
	int closestDepot = findClosestDepot(from);
	Car* c = new Car();
	if (!c->start(closestDepot, to, m_graph)) {
		return false;
	}
	//LE PB EST ICI !!
	m_lCar->push_back(c);
	cout << "ended ask for course \n";
	return true;
}

void CourseManager::run()
{
	cout << "run \n";
	for (int i = 0; i < m_lCar->size(); i++) {
		if (m_lCar->at(i)->moveForward(1)) {
			if (m_lCar->at(i)->getMode())
				free(m_lCar);
			else {
				int currentNode = m_lCar->at(i)->getNextNode();
				m_lCar->at(i)->setOnReturnMode();
				m_lCar->at(i)->start(currentNode, findClosestDepot(currentNode), m_graph);
			}
		} 
	}
	cout << "end run \n";
}
