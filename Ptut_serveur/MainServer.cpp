#include "MainServer.h"
#include <vector>
#include <seasocks/WebSocket.h>
#include <zconf.h>
#include "Encodeur.h"
#include "NetworkManager.h"

using namespace seasocks;



using namespace std;


MainServer::MainServer(Application* application) {
	m_application = application;
	m_graph = application->getGraph();

    auto h = std::make_shared<myHandler>(myHandler{});
    handler = h.get();
    handler->m_graph = m_graph;
    handler->m_application = m_application;
    server = NetworkManager::openConnection(h, "/home/etienne/Bureau/projet-tut/webApp/dist/webApp", 20999, t1);

}

void MainServer::mainloop()
{
	//implement main loop
    while(true){

        m_application->moveForward(0.5);

        server->execute([this]{
            for(auto c : handler->connections) {

                vector<Car*> lcars = m_application->getCars();
                for(Car* car : lcars){

                    string msg;
                    float angle=car->getAngle();
                    if(angle<0)
                        angle+=3.14*2;
                    msg = Encodeur::UpdateCar(car->getMId(),0, car->getX(), car->getY(), angle * 1000, car->getSpeed());
                    c->send(msg.c_str());

                }

            }
        });

        usleep(30000);
    }
}

void MainServer::selectCars(int x0, int y0, int fenX, int fenY, vector<Car*>* lCars)
{
	Car* c;
	for (int i = 0; i < m_application->getCars().size(); i++) {
		c = m_application->getCars()[i];
		if (c->getX() >= x0 && c->getX() <= x0 + fenX &&
			c->getY() >= y0 && c->getY() <= y0 + fenY) {
			lCars->push_back(c);
		}
	}
}

void MainServer::selectEdges(int x0, int y0, int fenX, int fenY, vector<Edge>* lEdges)
{//fait un peu crade possibilite d'opti
	Edge e;
	for (int i = 0; i < m_graph->getlElement().size(); i++) {
		for (int j = 0; j < m_graph->getlElement()[i].size(); j++) {
			e = m_graph->getlElement()[i][j];
			if (e.getX() >= x0 - e.getLength() && e.getX() <= x0 + fenX + e.getLength() and
				e.getY() >= y0 - e.getLength() && e.getY() <= y0 + e.getLength() + fenY) {
				lEdges->push_back(e);
			}
		}
	}
}
