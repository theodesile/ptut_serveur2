#include "Segment.h"
#include <cmath>
#include "Point.h"

Segment::Segment(int xA_, int xB_, int yA_, int yB_)
{
	xA = xA_;
	xB = xB_;
	yA = yA_;
	yB = yB_;
	calcAngle();
}

int Segment::isIntersect(Segment* s)
{
	//int intersectsegment(Point A, Point B, Point I, Point P)
		Point D, E;
		Point 
		D.x = B.x - A.x;
		D.y = B.y - A.y;
		E.x = P.x - I.x;
		E.y = P.y - I.y;
		double denom = D.x * E.y - D.y * E.x;
		if (denom == 0)
			return -1;   // erreur, cas limite
		t = -(A.x * E.y - I.x * E.y - E.x * A.y + E.x * I.y) / denom;
		if (t < 0 || t >= 1)
			return 0;
		u = -(-D.x * A.y + D.x * I.y + D.y * A.x - D.y * I.x) / denom;
		if (u < 0 || u >= 1)
			return 0;
		return 1;
}

void Segment::calcAngle()
{
	float l = sqrt(pow(xB - xA, 2) + pow(yB - yA, 2));
	float angle = acos(l / abs(xB - xA));
}
