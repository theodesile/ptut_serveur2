//
// Created by etienne on 03/11/2019.
//

#ifndef MYHANDLER_H
#define MYHANDLER_H

#include "Encodeur.h"

using namespace seasocks;

struct myHandler : seasocks::WebSocket::Handler {

    int nbrRequetes = 0;
    Graph* m_graph;
    Application* m_application;

    std::set<WebSocket *> connections;

    void onConnect(WebSocket *socket) override{
        connections.insert(socket);

        auto lroads = m_graph->getlElement();

        for(const vector<Edge>& roads : lroads){
            for(Edge road : roads) {

//                string msg;
//                float angle=road.getAngle();
//                if(angle<0)
//                    angle+=3.14*2;
//                if(road.getRadius() == 0)
//                    msg = Encodeur::UpdateRoad(road.getMId(), road.getX(), road.getY(), angle * 1000, road.getLength(), true);
//                else
//                    msg = Encodeur::UpdateRoad(road.getMId(), road.getX(), road.getY(), angle * 1000, road.getLength(), road.getRadius(),true);
//
//                socket->send(msg.c_str());
            }
        }

    }

    void onData(WebSocket *socket, const char *data) override{

        nbrRequetes++;
        string decoded = Encodeur::Decode(data);

        // DEBUG
        if(decoded.substr(0, 6) == "debug:")
            for(auto c : connections)
                c->send(decoded.substr(6, decoded.length()-7));
        // END DEBUG

        printf("Requete numero %i : %s\n", nbrRequetes, decoded.c_str());


        // ICICICICIICICICI
        printf("%s \n", decoded.c_str());
        if(decoded.substr(4, 1) == "6"){
            int src, dest;
            sscanf(decoded.c_str(), "UPD_6{%d,%d}", &src, &dest);
            printf("%d %d \n", src, dest);
            m_application->commandCourse(src, dest);
        }

    }

    void onDisconnect(WebSocket *socket) override{
        connections.erase(socket);
    }
};


#endif //MYHANDLER_H
