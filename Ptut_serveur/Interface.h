#pragma once
#include <SFML/Graphics.hpp>
#include "Application.h"
#include "Car.h"
#include <SFML/Graphics.hpp>
/*
Classe qui permet de cr�er une interface graphique de test de l'application
*/
class Interface : sf::RenderWindow
{
public:
	Interface(Application* application);
private:
	Application* m_application;
	void start();
	void drawRoad(Edge* edge);
	void drawCar(Car* car);
	void drawAllRoad();
	std::vector<Car>* m_lCar;
	sf::Texture* texture;
	sf::Sprite m_background;
	sf::Texture m_backgroundTexture;
};