#include "GraphicalCar.h"
#include <SFML/Graphics.hpp>

GraphicalCar::GraphicalCar(Car* car) : sf::RectangleShape(sf::Vector2f(car->getX(), car->getY())) {
	m_car = car;
	move();
}
void GraphicalCar::move() {
	const float PI = 3.141592654f;
	rotate(m_car->getAngle() / PI * 180);
	setPosition(m_car->getX(), m_car->getY());
}