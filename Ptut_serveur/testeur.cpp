//
// Created by etienne on 12/09/2019.
//

#include <cstdio>
#include <string>
#include <bitset>
#include <iostream>
#include <arpa/inet.h>
#include <unistd.h>
#include <thread>

#include "Encodeur.h"
#include "NetworkManager.h"




int main(int argc, char** argv){

    if(argc < 2)
        argv[1] = "web";

    printf("Lancement du programme de test...\n\n");

/*

    string coded = Encodeur::Delete(25);
    cout << coded << endl;
    cout << "Codé sur " << coded.length() << " octets" << endl;
    cout << Encodeur::Decode(coded) << endl << endl;

    string coded2 = Encodeur::UpdateRoad(787, 665.22, 101.45, 996, 560);
    cout << coded2 << endl;
    cout << "Codé sur " << coded2.length() << " octets" << endl;
    cout << Encodeur::Decode(coded2) << endl << endl;

    string coded3 = Encodeur::UpdateRoad(787, 73.772, 5.36, 12, 5555, 1250);
    cout << coded3 << endl;
    cout << "Codé sur " << coded3.length() << " octets" << endl;
    cout << Encodeur::Decode(coded3) << endl << endl;

    string coded4 = Encodeur::UpdateCar(787, 15, 73.772, 5.36, 12, 9);
    cout << coded4 << endl;
    cout << "Codé sur " << coded4.length() << " octets" << endl;
    cout << Encodeur::Decode(coded4) << endl << endl;

    string coded5 = Encodeur::UpdatePedestrian(45, 8, 889.3, 182.004, 13, 2);
    cout << coded5 << endl;
    cout << "Codé sur " << coded5.length() << " octets" << endl;
    cout << Encodeur::Decode(coded5) << endl << endl;

    string coded6 = Encodeur::AddFurniture(0, 588, 2654.369, 850);
    cout << coded6 << endl;
    cout << "Codé sur " << coded6.length() << " octets" << endl;
    cout << Encodeur::Decode(coded6) << endl << endl;


    cout << Encodeur::UpdateCar(787, 15, 73.772, 5.36, 12, 9) << endl;

*/

//    string coded = Encodeur::AddRoad(12.5, 45.98, 28, 455, true);
//    printf("%s", (coded + "\n").c_str());
//    string decoded = Encodeur::Decode(coded);
//    printf("%s", (decoded + "\n").c_str());

    struct myHandler : seasocks::WebSocket::Handler {

        int nbrRequetes = 0;

        std::set<WebSocket *> connections;

        void onConnect(WebSocket *socket) override{
            connections.insert(socket);
            socket->send(Encodeur::AddRoad(12.5, 45.98, 28, 455, true));
        }

        void onData(WebSocket *socket, const char *data) override{
            nbrRequetes++;
            printf("Requete numero %i : %s\n", nbrRequetes, Encodeur::Decode(data).c_str());
        }

        void onDisconnect(WebSocket *socket) override{
            connections.erase(socket);
        }
    };

    struct myChatHandler : seasocks::WebSocket::Handler {

        std::set<WebSocket *> connections;

        void onConnect(WebSocket *socket) override{
            connections.insert(socket);
            socket->send("Bonjour, envoi ton message !");
        }

        void onData(WebSocket *socket, const char *data) override{
            printf("Requete : %s\n", data);
            for(auto c : connections)
                c->send("<span style='color: rgb(111, 0, 255);'>" + (string)NetworkManager::getAdress(socket) + ":</span> " + data);
        }

        void onDisconnect(WebSocket *socket) override{
            connections.erase(socket);
        }

    };

    // = = = Test réseau = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    thread t;

    auto handler = std::make_shared<myHandler>(myHandler{});
    auto server = NetworkManager::openConnection(handler, std::make_shared<myChatHandler>(myChatHandler{}), argv[1], 20999, t);

    while(true){
        string str = Encodeur::UpdateCar(0,0,rand()%400-200,rand()%400-200,0,rand()%300);
        printf("Send %s\n", Encodeur::Decode(str).c_str());

        server->execute([handler, str]{
            for(auto c : handler->connections)
                c->send(str);
        });

        sleep(1);
    }


    getchar();

    server->terminate();

}